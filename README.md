# Detecting and Alerting almost any kind of `INTRUSION` activity which may harm your system.

***So here is the attacker's system from where he is performing an attack which basically scans the port and services of the victims's system.***

![kali](/uploads/28a03e5b8049b278b8bb9c5271c387c3/kali.PNG)


***And this is the victims's system where IDS is activated and detected the attacks or scans performed by the attacker. Basically its an alert and
based on this, victim can secure and defend its system by knowing what type of attack can be done on its system.***

![ubuntu__2_](/uploads/827163fe7bf785c7e598ce97db4e500b/ubuntu__2_.PNG)

### This is just a `README` file, the complete project is in private 